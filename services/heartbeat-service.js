let ElasticAdapter = require("../modules/elastic-adapter");
let Tables = require("../modules/tables");
const async = require('async');
const HEARTRATE = require("../import/heartrate-sample.json");
const fs = require('fs');
let csvToJson = require('convert-csv-to-json');


var HeartbeatServices = function (app) {

    this.app = app;
    this.elasticAdapter = new ElasticAdapter(app);
    this.tables = new Tables(app);
}

module.exports = HeartbeatServices;

//**********************************
//Walking CRUD Services
//**********************************

HeartbeatServices.prototype.add = function (data, cbk) {

    var self = this;
    var inputObj = data;

    self.elasticAdapter.insert(this.tables.HEARTBEAT_TABLE, inputObj, function (status, result) {
        cbk(status, result);
    });
}


HeartbeatServices.prototype.import = function (req, cbk) {

    var self = this;
    let data_arr = csvToJson.fieldDelimiter(',').getJsonFromCsv('./import/HEARTRATE_AUTO_1623697766022.csv');

    async.mapSeries(data_arr,function (oneItem, fCbk) {
        var insertData = {};
        insertData['date'] = oneItem.date;
        insertData['time'] = oneItem.time;
        insertData['heartrate'] = parseInt(oneItem.heartRate);
        insertData['userid'] = req.session.sessionObj["email"];
        insertData['createdtime'] = new Date().getTime();
        insertData['updatedtime'] = new Date().getTime();
        self.elasticAdapter.insert(self.tables.HEARTBEAT_TABLE, insertData, function (status, result) {
            console.log("heartbeat data inserting...");
            console.log(result);
            fCbk(null, null);
        });

    },function (err, res) {
        if(!err){
            cbk(true, "Success");
        }else{
            cbk(false, "Failed");
        }
    });
}

HeartbeatServices.prototype.list = function (data, cbk) {

    var self = this;

    let queryObj = data;

    self.elasticAdapter.search(this.tables.HEARTBEAT_TABLE, queryObj, function (status, result) {
        cbk(status, result);
    });
}

HeartbeatServices.prototype.update = function (data, cbk) {

    var self = this;
    var inputObj = data;
    var rec_id = inputObj._id;
    delete inputObj._id;

    self.elasticAdapter.update(this.tables.HEARTBEAT_TABLE, rec_id, inputObj, function (status, result) {
        cbk(status, result);
    });
}

HeartbeatServices.prototype.delete = function (data, cbk) {

    var self = this;
    var rec_id = data;

    self.elasticAdapter.delete(this.tables.HEARTBEAT_TABLE, rec_id, function (status, result) {
        cbk(status, result);
    });
}