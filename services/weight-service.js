var ElasticAdapter = require("../modules/elastic-adapter");
var Tables = require("../modules/tables");

var WeightServices = function (app) {

    this.app = app;
    this.elasticAdapter = new ElasticAdapter(app);
    this.tables = new Tables(app);
}

module.exports = WeightServices;

//**********************************
//Walking CRUD Services
//**********************************

WeightServices.prototype.add = function (data, cbk) {

    var self = this;
    var inputObj = data;

    self.elasticAdapter.insert(this.tables.WEIGHT_TABLE, inputObj, function (status, result) {
        cbk(status, result);
    });
}

WeightServices.prototype.list = function (data, cbk) {

    var self = this;

    let queryObj = data;

    self.elasticAdapter.search(this.tables.WEIGHT_TABLE, queryObj, function (status, result) {
        cbk(status, result);
    });
}

WeightServices.prototype.update = function (data, cbk) {

    var self = this;
    var inputObj = data;
    var rec_id = inputObj._id;
    delete inputObj._id;

    self.elasticAdapter.update(this.tables.WEIGHT_TABLE, rec_id, inputObj, function (status, result) {
        cbk(status, result);
    });
}

WeightServices.prototype.delete = function (data, cbk) {

    var self = this;
    var rec_id = data;

    self.elasticAdapter.delete(this.tables.WEIGHT_TABLE, rec_id, function (status, result) {
        cbk(status, result);
    });
}