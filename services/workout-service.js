let ElasticAdapter = require("../modules/elastic-adapter");
let Tables = require("../modules/tables");
const async = require('async');
const HEARTRATE = require("../import/heartrate-sample.json");
const fs = require('fs');
let csvToJson = require('convert-csv-to-json');

var WorkoutServices = function (app) {

    this.app = app;
    this.elasticAdapter = new ElasticAdapter(app);
    this.tables = new Tables(app);
}

module.exports = WorkoutServices;

//**********************************
//Walking CRUD Services
//**********************************

WorkoutServices.prototype.add = function (data, cbk) {

    var self = this;
    var inputObj = data;

    self.elasticAdapter.insert(this.tables.WORKOUT_TABLE, inputObj, function (status, result) {
        cbk(status, result);
    });
}

WorkoutServices.prototype.import = function (req, cbk) {

    var self = this;
    let data_arr = csvToJson.fieldDelimiter(',').getJsonFromCsv('./import/ACTIVITY_STAGE_1623697765385.csv');

    async.mapSeries(data_arr,function (oneItem, fCbk) {
        var insertData = {};
        insertData['date'] = oneItem.date;
        insertData['start'] = oneItem.start;
        insertData['stop'] = oneItem.stop;
        insertData['distance'] = oneItem.distance ? parseInt(oneItem.distance) : 0;
        insertData['calories'] = oneItem.calories ? parseInt(oneItem.calories) : 0;
        insertData['steps'] = oneItem.steps ? parseInt(oneItem.steps) : 0;
        insertData['userid'] = req.session.sessionObj["email"];
        insertData['createdtime'] = new Date().getTime();
        insertData['updatedtime'] = new Date().getTime();

        self.elasticAdapter.insert(self.tables.WORKOUT_TABLE, insertData, function (status, result) {
            console.log("WORKOUT data inserting...");
            console.log(result);
            fCbk(null, null);
        });

    },function (err, res) {
        if(!err){
            cbk(true, "Success");
        }else{
            cbk(false, "Failed");
        }
    });
}


WorkoutServices.prototype.list = function (data, cbk) {

    var self = this;

    let queryObj = data;

    self.elasticAdapter.search(this.tables.WORKOUT_TABLE, queryObj, function (status, result) {
        cbk(status, result);
    });
}

WorkoutServices.prototype.update = function (data, cbk) {

    var self = this;
    var inputObj = data;
    var rec_id = inputObj._id;
    delete inputObj._id;

    self.elasticAdapter.update(this.tables.WORKOUT_TABLE, rec_id, inputObj, function (status, result) {
        cbk(status, result);
    });
}

WorkoutServices.prototype.delete = function (data, cbk) {

    var self = this;
    var rec_id = data;

    self.elasticAdapter.delete(this.tables.WORKOUT_TABLE, rec_id, function (status, result) {
        cbk(status, result);
    });
}