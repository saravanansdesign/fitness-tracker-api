var ElasticAdapter = require("../modules/elastic-adapter");
var Tables = require("../modules/tables");

var UserService = function (app) {

    this.app = app;
    this.elasticAdapter = new ElasticAdapter(app);
    this.tables = new Tables(app);
}

module.exports = UserService;

//**********************************
//User CRUD Services
//**********************************

UserService.prototype.list = function (data, cbk) {

    var self = this;

    let queryObj = data;

    self.elasticAdapter.search(this.tables.USER_TABLE, queryObj, function (status, result) {
        cbk(status, result);
    });
}

UserService.prototype.view = function (data, cbk) {

    var self = this;
    var queryObj = data;

    self.elasticAdapter.search(this.tables.USER_TABLE, queryObj, function (status, result) {
        cbk(status, result);
    });
}

UserService.prototype.delete = function (data, cbk) {

    var self = this;
    var rec_id = data;

    self.elasticAdapter.delete(this.tables.USER_TABLE, rec_id, function (status, result) {
        cbk(status, result);
    });
}

UserService.prototype.signup = function (data, cbk) {

    var self = this;
    var inputObj = data;

    self.elasticAdapter.insert(this.tables.USER_TABLE, inputObj, function (status, result) {
        cbk(status, result);
    });
}

UserService.prototype.update = function (data, cbk) {

    var self = this;
    var inputObj = data;
    var rec_id = inputObj._id;
    delete inputObj._id;

    self.elasticAdapter.update(this.tables.USER_TABLE, rec_id, inputObj, function (status, result) {
        cbk(status, result);
    });
}