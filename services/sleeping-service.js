var ElasticAdapter = require("../modules/elastic-adapter");
var Tables = require("../modules/tables");

var SleepingServices = function (app) {

    this.app = app;
    this.elasticAdapter = new ElasticAdapter(app);
    this.tables = new Tables(app);
}

module.exports = SleepingServices;

//**********************************
//Walking CRUD Services
//**********************************

SleepingServices.prototype.add = function (data, cbk) {

    var self = this;
    var inputObj = data;

    self.elasticAdapter.insert(this.tables.SLEEPING_TABLE, inputObj, function (status, result) {
        cbk(status, result);
    });
}

SleepingServices.prototype.list = function (data, cbk) {

    var self = this;

    let queryObj = data;

    self.elasticAdapter.search(this.tables.SLEEPING_TABLE, queryObj, function (status, result) {
        cbk(status, result);
    });
}

SleepingServices.prototype.update = function (data, cbk) {

    var self = this;
    var inputObj = data;
    var rec_id = inputObj._id;
    delete inputObj._id;

    self.elasticAdapter.update(this.tables.SLEEPING_TABLE, rec_id, inputObj, function (status, result) {
        cbk(status, result);
    });
}

SleepingServices.prototype.delete = function (data, cbk) {

    var self = this;
    var rec_id = data;

    self.elasticAdapter.delete(this.tables.SLEEPING_TABLE, rec_id, function (status, result) {
        cbk(status, result);
    });
}