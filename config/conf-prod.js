var CONF = {
    "url" : "http://localhost:7021",
    "port" : 7021,
    "elastic" : {
        "url" : "localhost:9200",
        "apiVersion" :"7.2"
    }
};

module.exports = CONF;
