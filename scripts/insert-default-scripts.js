const request = require('request');
const moment = require('moment');
const async = require('async');
const _ = require('underscore');
const fs = require('fs');

/*******************************
 * Require Configuration
 ****************************/
var conf = {};
var platformConfig = {};
var env = '';

try {
    if(process.env.HOME){
        conf = require(process.env.HOME + '/config/boodskap-workflow-studio-config');
        platformConfig = require(process.env.HOME + '/config/workflow-studio-config');
        console.log(new Date() + ' | Workflow Studio Build Configuration Loaded From Config');
    }else{
        conf = require('./webapps/workflow-studio-config');
    }
} catch (e) {
    console.error(e);
    return;
}
const API_URL = "http://zedbee.boodskap.io/api";

var BUILD_STATUS = true;

async.series({
    insertFarmersMarket : function(tCbk){
        logger('WORKFLOW-PROCESS' , 'Loading default Workflow Process');

        var tFlag = true;

        fs.readFile('./import/process.json', function (err, data) {
            if (err) {
                logger('ERROR', err)
                logger('WORKFLOW-PROCESS', 'Error in loading default Workflow Process!')
                tCbk(err, null)
            } else {
                var str = data.toString();
                var obj = JSON.parse(str);

                async.mapSeries(obj.hits.hits,function (dat, fCbk) {

                    var insertData = dat._source;
                    insertData['id'] = insertData.id;
                    insertData['name'] = insertData.name;
                    insertData['language'] = insertData.language;
                    insertData['code'] = insertData.code;
                    insertData['description'] = insertData.description;
                    insertData['group'] = insertData.group;
                    insertData['domainKey'] = DOMAIN_KEY;
                    insertData['tags'] = insertData.tags;
                    insertData['input'] = insertData.input;
                    insertData['output'] = insertData.output;
                    insertData['properties'] = insertData.properties;
                    // insertData['create_ts'] = new Date().getTime();
                    // insertData['last_updt_ts'] = new Date().getTime();
                    // insertData['last_updt_nam'] = 'BUILD';

                    insertDynamicProcess(insertData, function (status, result){
                        console.log(insertData.id,status);

                        if(!status){
                            tFlag = false;
                        }
                        fCbk(null, null)
                    })
                },function (er, res) {

                    if(tFlag) {
                        logger('WORKFLOW-PROCESS' , 'Default Workflow Process created successfully');
                    }else{
                        logger('WORKFLOW-PROCESS' , 'Error in creating default Workflow Process');
                    }
                    tCbk(null, null)
                })
            }
        });

    }
}, function (err,result) {

    if(!BUILD_STATUS){
        logger('ERROR' , 'Default Workflow Process Process Ended with error!')
        console.log("******************* ERROR OCCURRED ***************************")
    }else{
        logger('BUILD' , 'Default Workflow Process Process Ended!')
        console.log("******************* BUILD SUCCESS ***************************")
    }
});

function logger(module, msg){
    console.log(new Date() + ' | ['+module+'] ',msg)
}

//Platform API's
function insertRecord (rid, rkey, data, cbk) {

    request.post({
        uri: API_URL + '/record/insert/static/' + API_TOKEN +'/'+rid +'/'+rkey,
        headers: {'content-type': 'text/plain'},
        body: JSON.stringify(data),
    }, function (err, res, body) {

        if(!err) {

            if (res.statusCode === 200) {
                var result =  JSON.parse(res.body);
                cbk(true,null)
            } else {
                BUILD_STATUS = false;
                logger('ERROR',res.body)
                cbk(false, JSON.parse(res.body))
            }
        }else{
            BUILD_STATUS = false;
            logger('ERROR',err)
            cbk(false,null)
        }

    });
}

function insertDynamicProcess (data, cbk) {
    request.post({
        uri: API_URL + '/process/upsert/' + API_TOKEN,
        headers: {'content-type': 'application/json'},
        body: JSON.stringify(data),
    }, function (err, res, body) {

        if(!err) {

            if (res.statusCode === 200) {
                var result =  JSON.parse(res.body);
                cbk(true,null)
            } else {
                BUILD_STATUS = false;
                logger('ERROR',res.body)
                cbk(false, JSON.parse(res.body))
            }
        }else{
            BUILD_STATUS = false;
            logger('ERROR',err)
            cbk(false,null)
        }

    });
}

function upsertUser(data, cbk) {

    request.post({
        uri: API_URL + '/user/upsert/' + API_TOKEN,
        headers: {'content-type': 'application/json'},
        body: JSON.stringify(data),
    }, function (err, res, body) {

        if(!err) {

            if (res.statusCode === 200) {
                cbk(true, JSON.parse(res.body))
            } else {
                BUILD_STATUS = false;
                logger('ERROR',res.body)
                cbk(false, res.body)
            }
        }else{
            BUILD_STATUS = false;
            logger('ERROR',err)
            cbk(false,null)
        }

    });
}