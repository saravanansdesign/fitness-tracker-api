var Auth = require("../controllers/auth-controller");
var Users = require("../controllers/user-controller");
var Walking = require("../controllers/walking-controller");
var Sleeping = require("../controllers/sleeping-controller");
var Workout = require("../controllers/workout-controller");
var Weight = require("../controllers/weight-controller");
var Height = require("../controllers/height-controller");
var Heartbeat = require("../controllers/heartbeat-controller");

var API_ROUTES = function(app){

    this.app = app;
    this.auth = new Auth(app);
    this.users = new Users(app);
    this.walking = new Walking(app);
    this.sleeping = new Sleeping(app);
    this.workout = new Workout(app);
    this.weight = new Weight(app);
    this.height = new Height(app);
    this.heartbeat = new Heartbeat(app);
};

API_ROUTES.prototype.init = function () {

    const self = this;
    const app = this.app;

    var sessionCheck = function (req, res, next) {

        var sessionObj = req.session['sessionObj'];
        if (sessionObj) {
            next();
        } else {
            res.status(401).json({ status: false, message: 'Unauthorized Access' });
        }
    };

    app.post('/auth/:action', function (req, res) {
        self.auth.performAction(req, res);
    });

    app.post('/user/:action',sessionCheck, function(req, res) {
        self.users.performAction(req, res);
    });

    app.post('/walking/:action',sessionCheck, function(req, res) {
        self.walking.performAction(req, res);
    });

    app.post('/weight/:action',sessionCheck, function(req, res) {
        self.weight.performAction(req, res);
    });

    app.post('/sleeping/:action',sessionCheck, function(req, res) {
        self.sleeping.performAction(req, res);
    });

    app.post('/workout/:action',sessionCheck, function(req, res) {
        self.workout.performAction(req, res);
    });

    app.post('/height/:action',sessionCheck, function(req, res) {
        self.height.performAction(req, res);
    });

    app.post('/heartbeat/:action',sessionCheck, function(req, res) {
        self.heartbeat.performAction(req, res);
    });
}

module.exports = API_ROUTES;