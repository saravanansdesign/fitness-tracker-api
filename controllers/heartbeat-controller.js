var HeartbeatServices = require('../services/heartbeat-service');

var moment = require('moment');

var HeartbeatController = function (app) {

    this.app = app;
    this.conf = app.conf;
    this.heartbeatServices = new HeartbeatServices(app);
};

module.exports = HeartbeatController;

HeartbeatController.prototype.performAction = function (req, res) {

    const self = this;

    var action = req['params']['action'];

    switch (action) {
        case "add":
            self.add(req, res);
            break;

        case "list":
            self.list(req, res);
            break;

        case "update":
            self.update(req, res);
            break;

        case "delete":
            self.delete(req, res);
            break;

        case "import":
            self.import(req, res);
            break;

        default:
            res.status(401).json({ status: false, message: 'Unauthorized Access' })
    }
};

//**********************************
//User CRUD Operations
//**********************************

HeartbeatController.prototype.add = function (req, res) {

    const self = this;
    let queryObj = req.body;

    if (queryObj) {

        self.heartbeatServices.add(queryObj, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get" , result : result});
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}

HeartbeatController.prototype.list = function (req, res) {

    const self = this;
    let queryObj = req.body;

    if (queryObj) {

        self.heartbeatServices.list(queryObj, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get" , result : result});
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}

HeartbeatController.prototype.update = function (req, res) {

    const self = this;
    let queryObj = req.body;

    if (queryObj) {

        self.heartbeatServices.update(queryObj, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get" , result : result});
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}

HeartbeatController.prototype.delete = function (req, res) {

    const self = this;
    let inputObj = req.body;

    if (inputObj._id) {

        self.heartbeatServices.delete(inputObj._id, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get" , result : result});
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}

HeartbeatController.prototype.import = function (req, res) {

    const self = this;
    let queryObj = req.body;

    if (queryObj) {

        self.heartbeatServices.import(req, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get" , result : result});
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}
