var AuthServices = require('../services/auth-service');
var UserServices = require('../services/user-service');

var moment = require('moment');

var Auth = function (app) {

    this.app = app;
    this.conf = app.conf;
    this.userServices = new UserServices(app);
    this.authServices = new AuthServices(app);
};

module.exports = Auth;

Auth.prototype.performAction = function (req, res) {

    const self = this;

    var action = req['params']['action'];

    switch (action) {

        //--------------------------
        //User Login Services
        //--------------------------
        case "login":
            self.login(req, res);
            break;
        case "google-login":
            self.login(req, res);
            break;
        case "twitter-login":
            self.login(req, res);
            break;
        case "facebook-login":
            self.login(req, res);
            break;
        case "instagram-login":
            self.login(req, res);
            break;

        //--------------------------
        //User Sign Up Services
        //--------------------------
        case "signup":
            self.login(req, res);
            break;
        case "facebook-signup":
            self.login(req, res);
            break;
        case "instagram-signup":
            self.login(req, res);
            break;
        case "google-signup":
            self.login(req, res);
            break;
        case "twitter-signup":
            self.login(req, res);
            break;

        //--------------------------
        //Other Auth Services
        //--------------------------
        case "forgot-password":
            self.forgotPassword(req, res);
            break;
        case "change-password":
            self.changePassword(req, res);
            break;
        case "session-check":
            self.sessionCheck(req, res);
            break;
        case "logout":
            self.logout(req, res);
            break;
        default:
            res.status(401).json({ status: false, message: 'Unauthorized Access' })
    }
};


//**********************************
//Authentication Operations
//**********************************

Auth.prototype.login = function (req, res) {

    const self = this;
    var reqObj = req.body;

    if(!reqObj.email){
        res.json({ status: false, message: "Email address is required fields!" });

    }else if(!reqObj.password){
        res.json({ status: false, message: "Password is required fields!" });

    }else{

        self.authServices.getUser(reqObj, function (status, result) {
            if (status) {
                if(result["data"]["data"].length > 0){
                    req.cookies['fitness_tracker'] = result["data"]["data"][0];
                    req.session['sessionObj'] = result["data"]["data"][0];
                    res.json({ status: true, message : "success", result: result["data"]["data"][0] });
                }else{
                    res.json({ status: false, message: "Error to get user list"});
                }
            } else {
                res.json({ status: false, message: "Error to get user list" , result : result});
            }
        })
    }
}

Auth.prototype.signup = function (req, res) {

    const self = this;
    let reqObj = req.body;

    if (reqObj && reqObj.email) {
        var queryObj = {
            "query" : {
                "match_phrase" : {
                    "email": reqObj.email
                }
            }
        };

        self.userServices.list(queryObj, function (status, result) {

            if (status) {

                if(result.data.data.length > 0){
                    res.json({ status: true, message : "User already exist!", result: result })
                }else{
                    self.userServices.signup(reqObj, function (status, result) {
                        if (status) {
                            res.json({ status: true, message : "Sign up completed successfully!", result: result })
                        } else {
                            res.json({ status: false, message: "Error to get user list" });
                        }
                    });
                }

            } else {
                res.json({ status: false, message: "Error to get user list" });
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}

Auth.prototype.sessionCheck = function (req, res) {

    const self = this;
    let reqObj = req.body;
}

Auth.prototype.forgotPassword = function (req, res) {

    const self = this;
    let queryObj = req.body;
}

Auth.prototype.changePassword = function (req, res) {

    const self = this;
    let reqObj = req.body;
}

Auth.prototype.logout = function (req, res) {

    req.session.sessionObj = "";
    res.clearCookie('fitness_tracker');
}