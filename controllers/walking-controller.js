var WalkingServices = require('../services/walking-service');

var moment = require('moment');

var WalkingController = function (app) {

    this.app = app;
    this.conf = app.conf;
    this.walkingServices = new WalkingServices(app);
};

module.exports = WalkingController;

WalkingController.prototype.performAction = function (req, res) {

    const self = this;

    var action = req['params']['action'];

    switch (action) {
        case "add":
            self.add(req, res);
            break;

        case "list":
            self.list(req, res);
            break;

        case "update":
            self.update(req, res);
            break;

        case "delete":
            self.delete(req, res);
            break;

        default:
            res.status(401).json({ status: false, message: 'Unauthorized Access' })
    }
};

//**********************************
//User CRUD Operations
//**********************************

WalkingController.prototype.add = function (req, res) {

    const self = this;
    let queryObj = req.body;

    if (queryObj) {

        self.walkingServices.add(queryObj, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get" , result : result});
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}

WalkingController.prototype.list = function (req, res) {

    const self = this;
    let queryObj = req.body;

    if (queryObj) {

        self.walkingServices.list(queryObj, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get" , result : result});
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}

WalkingController.prototype.update = function (req, res) {

    const self = this;
    let queryObj = req.body;

    if (queryObj) {

        self.walkingServices.update(queryObj, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get" , result : result});
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}

WalkingController.prototype.delete = function (req, res) {

    const self = this;
    let inputObj = req.body;

    if (inputObj._id) {

        self.walkingServices.delete(inputObj._id, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get" , result : result});
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}