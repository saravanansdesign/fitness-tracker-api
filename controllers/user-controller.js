var UserServices = require('../services/user-service');

var moment = require('moment');

var UserController = function (app) {

    this.app = app;
    this.conf = app.conf;
    this.userServices = new UserServices(app);
};

module.exports = UserController;

UserController.prototype.performAction = function (req, res) {

    const self = this;

    var action = req['params']['action'];

    switch (action) {
        case "add":
            self.add(req, res);
            break;
        case "list":
            self.list(req, res);
            break;
        case "delete":
            self.delete(req, res);
            break;
        case "update":
            self.update(req, res);
            break;
        default:
            res.status(401).json({ status: false, message: 'Unauthorized Access' })
    }
};

//**********************************
//User CRUD Operations
//**********************************
UserController.prototype.add = function (req, res) {

    const self = this;
    let reqObj = req.body;

    if (reqObj && reqObj.email) {
        var queryObj = {
            "query" : {
                "match_phrase" : {
                    "email": reqObj.email
                }
            }
        };

        self.userServices.list(queryObj, function (status, result) {

            if (status) {

                if(result.data.data.length > 0){
                    res.json({ status: true, message : "User already exist!", result: result })
                }else{
                    self.userServices.signup(reqObj, function (status, result) {
                        if (status) {
                            res.json({ status: true, message : "Sign up completed successfully!", result: result })
                        } else {
                            res.json({ status: false, message: "Error to get user list" });
                        }
                    });
                }

            } else {
                res.json({ status: false, message: "Error to get user list" });
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}

UserController.prototype.list = function (req, res) {

    const self = this;
    let queryObj = req.body;

    if (queryObj) {

        self.userServices.list(queryObj, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get user list" , result : result});
            }
        })
    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}

UserController.prototype.update = function (req, res) {

    const self = this;
    let reqObj = req.body;

    if (reqObj && reqObj._id) {

        self.userServices.update(reqObj, function (status, result) {
            if (status) {
                res.json({ status: true, message : "Update completed successfully!", result: result })
            } else {
                res.json({ status: false, message: "Error in Update" });
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}

UserController.prototype.delete = function (req, res) {

    const self = this;
    let reqObj = req.body;

    if (reqObj._id) {

        self.userServices.delete(reqObj._id, function (status, result) {
            if (status) {
                res.json({ status: true, message : "Deleted Successfully!", result: result })
            } else {
                res.json({ status: false, message: "Error in Delete" });
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}