var HeightServices = require('../services/height-service');

var moment = require('moment');

var HeightController = function (app) {

    this.app = app;
    this.conf = app.conf;
    this.heightServices = new HeightServices(app);
};

module.exports = HeightController;

HeightController.prototype.performAction = function (req, res) {

    const self = this;

    var action = req['params']['action'];

    switch (action) {
        case "add":
            self.add(req, res);
            break;

        case "list":
            self.list(req, res);
            break;

        case "update":
            self.update(req, res);
            break;

        case "delete":
            self.delete(req, res);
            break;

        default:
            res.status(401).json({ status: false, message: 'Unauthorized Access' })
    }
};

//**********************************
//User CRUD Operations
//**********************************

HeightController.prototype.add = function (req, res) {

    const self = this;
    let queryObj = req.body;

    if (queryObj) {

        self.heightServices.add(queryObj, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get" , result : result});
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}

HeightController.prototype.list = function (req, res) {

    const self = this;
    let queryObj = req.body;

    if (queryObj) {

        self.heightServices.list(queryObj, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get" , result : result});
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}

HeightController.prototype.update = function (req, res) {

    const self = this;
    let queryObj = req.body;

    if (queryObj) {

        self.heightServices.update(queryObj, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get" , result : result});
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}

HeightController.prototype.delete = function (req, res) {

    const self = this;
    let inputObj = req.body;

    if (inputObj._id) {

        self.heightServices.delete(inputObj._id, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get" , result : result});
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}