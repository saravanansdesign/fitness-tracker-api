var WorkoutServices = require('../services/workout-service');

var moment = require('moment');

var WorkoutController = function (app) {

    this.app = app;
    this.conf = app.conf;
    this.workoutServices = new WorkoutServices(app);
};

module.exports = WorkoutController;

WorkoutController.prototype.performAction = function (req, res) {

    const self = this;

    var action = req['params']['action'];

    switch (action) {
        case "add":
            self.add(req, res);
            break;

        case "import":
            self.import(req, res);
            break;

        case "list":
            self.list(req, res);
            break;

        case "update":
            self.update(req, res);
            break;

        case "delete":
            self.delete(req, res);
            break;

        default:
            res.status(401).json({ status: false, message: 'Unauthorized Access' })
    }
};

//**********************************
//User CRUD Operations
//**********************************

WorkoutController.prototype.add = function (req, res) {

    const self = this;
    let queryObj = req.body;

    if (queryObj) {

        self.workoutServices.add(queryObj, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get" , result : result});
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}

WorkoutController.prototype.import = function (req, res) {

    const self = this;
    let queryObj = req.body;

    if (queryObj) {

        self.workoutServices.import(req, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get" , result : result});
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}

WorkoutController.prototype.list = function (req, res) {

    const self = this;
    let queryObj = req.body;

    if (queryObj) {

        self.workoutServices.list(queryObj, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get" , result : result});
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}

WorkoutController.prototype.update = function (req, res) {

    const self = this;
    let queryObj = req.body;

    if (queryObj) {

        self.workoutServices.update(queryObj, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get" , result : result});
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}

WorkoutController.prototype.delete = function (req, res) {

    const self = this;
    let inputObj = req.body;

    if (inputObj._id) {

        self.workoutServices.delete(inputObj._id, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get" , result : result});
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}