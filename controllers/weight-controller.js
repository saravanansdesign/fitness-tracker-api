var WeightServices = require('../services/weight-service');

var moment = require('moment');

var WeightController = function (app) {

    this.app = app;
    this.conf = app.conf;
    this.weightServices = new WeightServices(app);
};

module.exports = WeightController;

WeightController.prototype.performAction = function (req, res) {

    const self = this;

    var action = req['params']['action'];

    switch (action) {
        case "add":
            self.add(req, res);
            break;

        case "list":
            self.list(req, res);
            break;

        case "update":
            self.update(req, res);
            break;

        case "delete":
            self.delete(req, res);
            break;

        default:
            res.status(401).json({ status: false, message: 'Unauthorized Access' })
    }
};

//**********************************
//User CRUD Operations
//**********************************

WeightController.prototype.add = function (req, res) {

    const self = this;
    let queryObj = req.body;

    if (queryObj) {

        self.weightServices.add(queryObj, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get" , result : result});
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}

WeightController.prototype.list = function (req, res) {

    const self = this;
    let queryObj = req.body;

    if (queryObj) {

        self.weightServices.list(queryObj, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get" , result : result});
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}

WeightController.prototype.update = function (req, res) {

    const self = this;
    let queryObj = req.body;

    if (queryObj) {

        self.weightServices.update(queryObj, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get" , result : result});
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}

WeightController.prototype.delete = function (req, res) {

    const self = this;
    let inputObj = req.body;

    if (inputObj._id) {

        self.weightServices.delete(inputObj._id, function (status, result) {
            if (status) {
                res.json({ status: true, message : "success", result: result })
            } else {
                res.json({ status: false, message: "Error to get" , result : result});
            }
        });

    } else {
        res.json({ status: false, message: "Please fill required fields!" });
    }
}