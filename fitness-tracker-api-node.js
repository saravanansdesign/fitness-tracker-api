var express = require('express');
const bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var conf = require('./conf');
var expressLayouts = require('express-ejs-layouts');
const cookieSession = require('cookie-session');
const expressSession = require('express-session');
var htmlEngine = require('ejs').renderFile;
var app = express();
app["conf"] = conf;

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
    host: conf.elastic.url,
    log: false,
    apiVersion: conf.elastic.apiVersion, // use the same version of your Elasticsearch instance
});

var sessionObj = {
    secret: 'FITNESS_TRACKER',
    resave: false,
    saveUninitialized: true,
    cookie: {
        secure: false,
        maxAge: 5 * 60 * 60 * 1000 //5 hours
    }
}

if (process.env.NODE_ENV === 'PROD') {
    app.set('trust proxy', 1) // trust first proxy
    sessionObj.cookie.secure = true // serve secure cookies
}
app.use(expressSession(sessionObj));

app.use(cookieParser());
client.ping({
    // ping usually has a 3000ms timeout
    requestTimeout: 1000
}, function (error) {
    if (error) {
        console.trace('elasticsearch cluster is down!');
    } else {
        console.log('Elasticsearch cluster is online');
    }
});
app["client"] = client;

var APIRoutes = require("./routes/api-routes");

app.set('views', __dirname + '/views');
app.use('/webapps', express.static('webapps'));
app.use('/css', express.static('webapps/css'));
app.use('/js', express.static('webapps/js'));
app.use('/library', express.static('webapps/library'));
app.use('/images', express.static('webapps/images'));

app.engine('html', htmlEngine);
app.set('view engine', 'html');
app.use(expressLayouts);

app.listen(conf.port, () => {
    console.log("===================================================");
    console.log("****************************************************");
    console.log("Fitness Tracker System API Node is Listening : "+conf.port);
    console.log("****************************************************");
    console.log("====================================================");
});

var apiRoutes = new APIRoutes(app);
apiRoutes.init();
