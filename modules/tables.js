var Tables = function (app) {

    this.USER_TABLE = "users";
    this.WALKING_TABLE = "walking";
    this.WORKOUT_TABLE = "workout";
    this.HEARTBEAT_TABLE = "heartbeat";
    this.WEIGHT_TABLE = "weight";
    this.HEIGHT_TABLE = "height";
    this.SLEEPING_TABLE = "sleeping";
};

module.exports = Tables;