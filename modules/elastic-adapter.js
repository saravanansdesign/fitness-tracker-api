var _ = require('underscore');

var ElasticAdapter = function (app) {

    this.app = app;
};

module.exports = ElasticAdapter;

ElasticAdapter.prototype.search = function (tableName, queryObj, cbk) {

    var self = this;

    this.app.client.search({
        index: tableName,
        body: queryObj
    }).then(function (result) {
        var resultObj = self.elasticQueryFormatter(result);
        cbk(true, resultObj);

    }, function (err) {
        var errObj = {
            status: false,
            message: "Elastic search error",
            error: err
        };
        console.log(err);
        cbk(false, errObj);
    });
}

ElasticAdapter.prototype.insert = function (tableName, inputObj, cbk) {

    var self = this;

    console.log("inputObj------");
    console.log(inputObj);

    this.app.client.create({
        index: tableName,
        type: 'default',
        id: self.generateUUID(),
        body: inputObj
    }).then(function (result) {

        cbk(true, result);

    }, function (err) {

        var errObj = {
            status: false,
            message: "Elastic insert error",
            error: err
        };
        console.log(err);
        cbk(false, errObj);
    });
}

ElasticAdapter.prototype.update = function (tableName, rec_id, inputObj, cbk) {

    var self = this;

    this.app.client.update({
        index: tableName,
        type: "default",
        id: rec_id,
        body: {
            "doc": inputObj,
            "detect_noop": false
        }
    }).then(function (result) {

        cbk(true, result);

    }, function (err) {

        var errObj = {
            status: false,
            message: "Elastic update error",
            error: err
        };
        console.log(err);
        cbk(false, errObj);
    });
}

ElasticAdapter.prototype.delete = function (tableName, rec_id, cbk) {

    var self = this;

    this.app.client.delete({
        index: tableName,
        type: "default",
        id: rec_id
    }).then(function (result) {

        cbk(true, result);

    }, function (err) {

        var errObj = {
            status: false,
            message: "Elastic search error",
            error: err
        };

        cbk(false, errObj);
    });
}

ElasticAdapter.prototype.elasticQueryFormatter = function (data) {

    var resultObj = {
        total: 0,
        data: {},
        aggregations: {}
    }
    var arrayData = data;

    var totalRecords = arrayData.hits.total ? arrayData.hits.total.value : 0;
    var records = arrayData.hits.hits;

    var aggregations = arrayData.aggregations ? arrayData.aggregations : {};

    var count = 0;

    var tempData = []

    for (var i = 0; i < records.length; i++) {
        if (records[i]['_id'] != '_search') {
            records[i]['_source']['_id'] = records[i]['_id'];
            tempData.push(records[i]['_source']);
        } else {
            count++;
        }
    }

    totalRecords = totalRecords > 0 ? totalRecords - count : 0

    resultObj = {
        "total": totalRecords,
        "data": {
            "recordsTotal": totalRecords,
            "recordsFiltered": totalRecords,
            "data": _.pluck(records, '_source')
        },
        aggregations: aggregations
    }

    return resultObj;
};

ElasticAdapter.prototype.s4 = function () {

    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
}

ElasticAdapter.prototype.generateUUID = function () {

    return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + this.s4() + this.s4();
};